:PROPERTIES:
:ID:       6c85c694-0fa7-4ebd-aa43-0321a5541cbe
:END:
#+title: Widerspruch
Sein-für-Anderes ist unbestimmte, affirmative Gemeinschaft von Etwas mit seinem Anderen; in der Grenze hebt[135] sich das Nichtsein-für-Anderes hervor, die qualitative Negation des Anderen, welches dadurch von dem in sich reflektierten Etwas abgehalten wird. Die Entwicklung dieses Begriffs ist zu sehen, welche sich aber vielmehr als Verwicklung und Widerspruch zeigt. Dieser ist sogleich darin vorhanden, daß die Grenze als in sich reflektierte Negation des Etwas die Momente des Etwas und des Anderen in ihr ideell enthält, und diese als unterschiedene Momente zugleich in der Sphäre des Daseins als reell, qualitativ unterschieden gesetzt sind.
