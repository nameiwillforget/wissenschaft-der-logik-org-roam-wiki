:PROPERTIES:
:ID:       9bc254ae-3856-4e88-b181-44601373020f
:END:
#+title: Vorstellen
[[file:~/.emacs.d/org-roam/Wissenschaft der Logik/Wissenschaft der Logik.org::Vorstellen][Anmerkung]]
Das Vorstellen ist ein Fürsichsein, in welchem die Bestimmtheiten nicht Grenzen und damit nicht ein Dasein, sondern nur Momente sind
