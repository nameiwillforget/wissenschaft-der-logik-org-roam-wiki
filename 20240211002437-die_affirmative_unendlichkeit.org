:PROPERTIES:
:ID:       3306da22-ef85-4316-9f30-5d43fcc13dac
:END:
#+title: Die affirmative Unendlichkeit 
[[file:Wissenschaft der Logik.org::*c. Die affirmative Unendlichkeit][c. Die affirmative Unendlichkeit]]
Infinity, as in, say, the infinite numbers from ℵ_0 onwards, arises at the limit of the finite, just in the classical way in which a limit sequence works, or the axiom of infinity, therefore is defined by the finite and furthermore is bounded by the finite, as in, if you imagine the infinite numbers as an interval, then it is half-closed with ℵ_0 its boundary. Therefore it is not truly infinite, (note that the German word "unendlich" literally means without end or boundary) as it has an end. Similarly, the finite numbers are only defined as finite by not being infinite, and furthermore, tend towards infinity. Therefore, both of these notions are intertwined and one goes into the other through negation, meaning complementation: the complement of the infinite numbers are the finite ones and the other way around. Hegel now says that this going into each other should be understood sequentially and forms "true infinity" as infinity going into finitude and then back into infinity infinitely often. I'll write it up more closely if I have the time. 

