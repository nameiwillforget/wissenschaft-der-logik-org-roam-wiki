:PROPERTIES:
:ID:       c8255b7c-753d-4d54-82e2-48e7a6b928e4
:ROAM_ALIASES: das entwickelte Unendliche des Verstandes
:END:
#+title: unendliche Progreß
Es ist eine leichte Forderung, welche, um die Natur des Unendlichen einzusehen, gemacht wird, das Bewußtsein zu haben, daß der unendliche Progreß, das entwickelte Unendliche des Verstandes,[167] die Beschaffenheit hat, die Abwechslung der beiden Bestimmungen, der Einheit und der Trennung beider Momente zu sein, und dann das fernere Bewußtsein zu haben, daß diese Einheit und diese Trennung selbst untrennbar sind.
