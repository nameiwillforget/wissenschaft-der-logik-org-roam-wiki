:PROPERTIES:
:ID:       fe92af75-a097-4303-b974-3f2e4b13cc02
:END:
#+title: Raum
Die Anwendung jedoch selbst gehört in die Betrachtung des Raums; um[138] sie hier anzudeuten, so ist der Punkt die ganz abstrakte Grenze, aber in einem Dasein, dieses ist noch ganz unbestimmt genommen, es ist der sogenannte absolute, d.h. abstrakte Raum, das schlechthin kontinuierliche Außereinandersein. Damit, daß die Grenze nicht abstrakte Negation, sondern in diesem Dasein, daß sie räumliche Bestimmtheit ist, ist der Punkt räumlich, der Widerspruch der abstrakten Negation und der Kontinuität und damit das Übergehen und Übergegangensein in Linie usf., wie es denn keinen Punkt gibt, wie auch nicht eine Linie und Fläche.

