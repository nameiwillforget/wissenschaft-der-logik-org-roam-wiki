:PROPERTIES:
:ID:       21c870ea-532e-4048-9049-718c6ccc0efd
:END:
#+title: Bestimmtwerden
So oder anders beschaffen ist Etwas als in äußerem Einfluß und Verhältnissen begriffen. Diese äußerliche Beziehung, von der die Beschaffenheit abhängt, und das Bestimmtwerden durch ein Anderes erscheint als etwas Zufälliges. Aber es ist Qualität des Etwas, dieser Äußerlichkeit preisgegeben zu sein und eine Beschaffenheit zu haben.


