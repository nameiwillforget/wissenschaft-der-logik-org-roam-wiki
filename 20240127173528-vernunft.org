:PROPERTIES:
:ID:       4fe69053-c89b-4eab-a5ab-83d895739329
:END:
#+title: Vernunft
Die Bestimmung des Menschen ist die denkende Vernunft: Denken überhaupt ist seine einfädle Bestimmtheit, er ist durch dieselbe von dem Tiere unterschieden; er ist Denken an sich, insofern dasselbe auch von seinem Sein-für-Anderes, seiner eigenen Natürlichkeit und Sinnlichkeit, wodurch er unmittelbar mit Anderem zusammenhängt, unterschieden ist. Aber das Denken ist auch an ihm; der Mensch selbst ist Denken, er ist da als denkend, es ist seine Existenz und Wirklichkeit; und ferner, indem es in seinem Dasein und sein Dasein im Denken ist, ist es konkret, ist mit Inhalt und Erfüllung zu nehmen, es ist denkende Vernunft, und so ist es [132] Bestimmung des Menschen. Aber selbst diese Bestimmung ist wieder nur an sich als ein Sollen, d. i. sie mit der Erfüllung, die ihrem Ansich einverleibt ist, in der Form des Ansich überhaupt gegen das ihr nicht einverleibte Dasein, das zugleich noch als äußerlich gegenüberstehende, unmittelbare Sinnlichkeit und Natur ist.


