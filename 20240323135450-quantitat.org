:PROPERTIES:
:ID:       61ba9966-46e9-4739-88e2-9ac76ee21d90
:END:
#+title: Quantität

Die Quantität [ist] die Bestimmtheit, die dem Sein [[id:88ee38be-6824-4d93-bddb-60363d5b9c7d][gleichgültig]] geworden, eine Grenze, die ebensosehr keine ist; das Fürsichsein, das schlechthin identisch mit dem Sein-für-Anderes, – die Repulsion der vielen Eins, die unmittelbar Nicht-Repulsion, Kontinuität derselben ist.
